package daniel.vieira.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

import daniel.vieira.Models.SolarStations_db;
import daniel.vieira.Models.TimePower_db;
import daniel.vieira.R;

/**
 * Created by danie on 12-Apr-17.
 */

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME = "SolarStations.db";

    private static final int DATABASE_VERSION = 1;

    Context context;

    Dao<SolarStations_db, Integer> solarStations_Dao = null;
    Dao<TimePower_db, Integer> timePower_Dao = null;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION, R.raw.ormlite_config);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, SolarStations_db.class);
            TableUtils.createTableIfNotExists(connectionSource, SolarStations_db.class);
            TableUtils.createTable(connectionSource, TimePower_db.class);
            TableUtils.createTableIfNotExists(connectionSource,TimePower_db.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        if (newVersion > oldVersion) {
            try {
                TableUtils.dropTable(connectionSource, SolarStations_db.class, true);
                TableUtils.dropTable(connectionSource, TimePower_db.class, true);
                onCreate(database, connectionSource);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void clearSolarStationsTable() {
        try {
            TableUtils.clearTable(connectionSource, SolarStations_db.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Dao<SolarStations_db,Integer> getSolarStations_Dao() throws SQLException{
        if( solarStations_Dao == null){
            solarStations_Dao = getDao(SolarStations_db.class);
        }
        return solarStations_Dao;
    }

    public Dao<TimePower_db,Integer> getTimePower_Dao() throws SQLException{
        if( timePower_Dao == null){
            timePower_Dao = getDao(TimePower_db.class);
        }
        return timePower_Dao;
    }

}
