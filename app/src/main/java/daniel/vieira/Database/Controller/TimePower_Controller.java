package daniel.vieira.Database.Controller;

import android.content.Context;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;

import org.androidannotations.annotations.EBean;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import daniel.vieira.Database.DatabaseHelper;
import daniel.vieira.Models.SolarStations_db;
import daniel.vieira.Models.TimePower_db;

/**
 * Created by danie on 22-Jun-17.
 */

@EBean
public class TimePower_Controller {

    DatabaseHelper helper;
    Dao<TimePower_db,Integer> dao = null;

    public TimePower_Controller(Context context){
        if (helper == null){
            helper = (DatabaseHelper) OpenHelperManager.getHelper(context, DatabaseHelper.class);
        }
        if (dao == null){
            try {
                dao = helper.getTimePower_Dao();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void createRead(TimePower_db read){
        try {
            dao.create(read);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void createColletion(Collection<TimePower_db> collection){
        try {
            dao.create(collection);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<TimePower_db> queryForDate(SolarStations_db solarStation, Date selectDay){
        List<TimePower_db> queryList = null;
        QueryBuilder<TimePower_db, Integer> queryBuilder = dao.queryBuilder();

        Date begining = new Date(selectDay.getYear(),selectDay.getMonth(),selectDay.getDate(),0,0,0); // getyear nao funciona
        Date ending = new Date(selectDay.getYear(),selectDay.getMonth(),selectDay.getDate(),23,59,59);

        try {
            queryBuilder.where().eq("solarstation_id", solarStation.getId()).and().between("time",begining,ending);
            PreparedQuery<TimePower_db> query = queryBuilder.prepare();
            queryList = dao.query(query);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }

        return queryList;
    }
}
