package daniel.vieira.Database.Controller;

import android.content.Context;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import daniel.vieira.Database.DatabaseHelper;
import daniel.vieira.Models.SolarStations_db;
import daniel.vieira.Models.TimePower_db;

/**
 * Created by danie on 12-Apr-17.
 */

@EBean
public class SolarStations_Controller {

    @Bean
    TimePower_Controller timePowerController;

    private DatabaseHelper helper = null;
    private Dao<SolarStations_db, Integer> dao = null;

    public SolarStations_Controller(Context context){
        if (helper == null){
            helper = (DatabaseHelper) OpenHelperManager.getHelper(context, DatabaseHelper.class);
        }
        if (dao == null){
            try {
                dao = helper.getSolarStations_Dao();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void createRead(SolarStations_db read){

        try {
            dao.create(read);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<SolarStations_db> getSolarStationsList(){
        List<SolarStations_db> readingList = null;

        try {
            readingList = dao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return readingList;
    }

    public SolarStations_db getSolarStation(int id){
        SolarStations_db reading = null;

        try {
            reading = dao.queryForId(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return reading;
    }

    public void deleteSolarStation(SolarStations_db read){
        try {
            dao.delete(read);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteAll(){
        try {
            dao.delete(dao.queryForAll());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void clearDB() {
        helper.clearSolarStationsTable();
    }

    public void createTestDB() {
        int tempYear = 2017 - 1900;

        SolarStations_db solarStations1 = new SolarStations_db("Casa Presidio", true, 2.5, 1043, "kWh");
        createRead(solarStations1);

        Collection<TimePower_db> tempCollection = new ArrayList<>();
        Date tempDate;
        TimePower_db tempO;
        for (int h = 0, min = 0; h < 6; min += 10){
            if (min != 60){
                tempDate = new Date(tempYear,6,22,h,min,0);
                tempO = new TimePower_db(solarStations1,tempDate,0);
                tempCollection.add(tempO);
            } else {
                h++;
                min = -10;
            }
        }
        int dayValues[] = {42,69,70,331,336,437,477,552,604,639,678,738,765,791,818,862,885,914,928,961,979,992,1002,1040,1060,1106,1196,1203,1145,1093,1072,217,968,1126,987,1059,958,1093,1034,877,977,959,945,890,868,849,817,769,751,722,695,628,595,545,516,446,411,373,338,268,213,181,150,47,42,33,26,16,10,7};
        for (int h = 6,min = 0, i = 0; h <= 16 || min <= 30; min += 10, i++){
            if (min != 60) {
                int powerValue = dayValues[i];
                tempDate = new Date(tempYear,6,22,h,min,0);
                tempO = new TimePower_db(solarStations1,tempDate,powerValue);
                tempCollection.add(tempO);
            } else {
                h++;
                min = -10;
                i--;
            }
        }
        for (int h = 17, min = 40; h <= 22 || min <= 50; min += 10){
            if (min != 60){
                tempDate = new Date(tempYear,6,22,h,min,0);
                tempO = new TimePower_db(solarStations1,tempDate,0);
                tempCollection.add(tempO);
            } else {
                h++;
                min = -10;
            }
        }
        timePowerController.createColletion(tempCollection);

        SolarStations_db solarStations2 = new SolarStations_db("Casa Iguape", false, 15,8, "kWh");
        createRead(solarStations2);
//        SolarStations_db solarQuery = getSolarStation(solarStations1.getId());
//        int size = solarQuery.getTimePower_table().size();
//        size = solarQuery.getTimePower_table().size();

    }
}
