package daniel.vieira.Adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.util.List;

import daniel.vieira.Database.Controller.SolarStations_Controller;
import daniel.vieira.Models.SolarStations_View;
import daniel.vieira.Models.SolarStations_View_;
import daniel.vieira.Models.SolarStations_db;

/**
 * Created by Cyro on 08/06/2017.
 */

@EBean
public class SolarStationsListAdapter extends BaseAdapter {

    @RootContext
    Context context;

    @Bean
    SolarStations_Controller solarStationsController;

    List<SolarStations_db> solarStations;

    @AfterInject
    protected void initAdapter(){
        solarStations = solarStationsController.getSolarStationsList();
    }

    @Override
    public int getCount() {
        return solarStations.size();
    }

    @Override
    public SolarStations_db getItem(int position) {
        return solarStations.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        SolarStations_View solarStationsView;
        if (convertView == null){
            solarStationsView = SolarStations_View_.build(context);
        }else {
            solarStationsView = (SolarStations_View) convertView;
        }

        solarStationsView.bind(getItem(position));

        return solarStationsView;
    }
}
