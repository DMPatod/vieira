package daniel.vieira.Activity;

import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.ViewById;

import daniel.vieira.Adapters.SolarStationsListAdapter;
import daniel.vieira.Database.Controller.SolarStations_Controller;
import daniel.vieira.Interfaces.Constants;
import daniel.vieira.Interfaces.SessionSingleton;
import daniel.vieira.Models.SolarStations_db;
import daniel.vieira.R;

@EActivity(R.layout.activity_solar_stations_list)
@OptionsMenu(R.menu.menu_solar_stations_list)
public class SolarStationsListActivity extends AppCompatActivity {

    @ViewById
    Toolbar toolbar;

    @ViewById
    ListView solarStationsListView;

    @Bean
    SolarStationsListAdapter listAdapter;

    @Bean
    SolarStations_Controller solarStationsController;

    @AfterViews
    protected void start(){
        solarStationsListView.setAdapter(listAdapter);
        configToolbar();
    }

    private void configToolbar(){
        toolbar.setTitle("Minhas Estações");
        setSupportActionBar(toolbar);
    }

    @ItemClick
    protected void solarStationsListViewItemClicked(SolarStations_db solarStation){

        SessionSingleton.getInstance().setSessionSolarStation(solarStation);

        Intent intent = new Intent(getApplicationContext(),BasicSolarInfosActivity_.class);
        startActivity(intent);

    }

    @OptionsItem(R.id.logOut)
    protected void logOut(){
        showConfirmLogoutDialog();
    }

    @Override
    public void onBackPressed() {
        showConfirmLogoutDialog();
    }

    private void showConfirmLogoutDialog(){
        final Dialog confirmDialog = new Dialog(this);
        confirmDialog.setTitle("TESTANDO");
        confirmDialog.setContentView(R.layout.dialog_confirm_logout);

        Button confirmDialogBt = (Button) confirmDialog.findViewById(R.id.confirmDialogBt);

        confirmDialogBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logoutAction(confirmDialog);
            }
        });

        Button dontConfirmDialogBt = (Button) confirmDialog.findViewById(R.id.dontconfirmDialogBt);

        dontConfirmDialogBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmDialog.dismiss();
            }
        });

        confirmDialog.show();
    }

    private void logoutAction(Dialog dialog){
        solarStationsController.clearDB();

        dialog.dismiss();

        Intent intent = new Intent(getApplicationContext(),LoginActivity_.class);
        startActivity(intent);
        finish();
    }
}
