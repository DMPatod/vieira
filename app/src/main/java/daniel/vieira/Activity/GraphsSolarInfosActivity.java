package daniel.vieira.Activity;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import daniel.vieira.Activity.Fragments.HistGraphFragment_;
import daniel.vieira.Activity.Fragments.PotGraphFragment_;
import daniel.vieira.Interfaces.SessionSingleton;
import daniel.vieira.Models.SolarStations_db;
import daniel.vieira.R;

@EActivity(R.layout.activity_graphs_solar_infos)
public class GraphsSolarInfosActivity extends AppCompatActivity {

    @ViewById
    Toolbar toolbar;

    @ViewById
    ViewPager graphsPagerView;

    @ViewById
    TabLayout graphsTabLayout;

    @AfterViews
    protected void run(){
        setupActivity();
    }

    private void setupActivity(){
        configToolbar();

        setupPagerView(graphsPagerView);

        graphsTabLayout.setupWithViewPager(graphsPagerView);
        graphsTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                graphsPagerView.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void configToolbar(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void setupPagerView(ViewPager viewPager){
        Fragment potGraphFragment = new PotGraphFragment_();
        Fragment histGraphFragment = new HistGraphFragment_();

        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());

        viewPagerAdapter.addFragment(potGraphFragment, "PotGraph");
        viewPagerAdapter.addFragment(histGraphFragment, "histGraph");

        viewPager.setAdapter(viewPagerAdapter);
    }

    private class ViewPagerAdapter extends FragmentPagerAdapter{
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager fragmentManager){
            super(fragmentManager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title){
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position){
            return mFragmentTitleList.get(position);
        }
    }
}
