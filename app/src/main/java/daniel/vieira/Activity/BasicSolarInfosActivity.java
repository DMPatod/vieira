package daniel.vieira.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.ViewById;

import daniel.vieira.Database.Controller.SolarStations_Controller;
import daniel.vieira.Interfaces.Constants;
import daniel.vieira.Interfaces.SessionSingleton;
import daniel.vieira.Models.SolarStations_db;
import daniel.vieira.R;

@EActivity(R.layout.activity_basic_solar_infos)
@OptionsMenu(R.menu.menu_basic_solar_infos)
public class BasicSolarInfosActivity extends AppCompatActivity {

    @ViewById
    Toolbar toolbar;

    @ViewById(R.id.iv_binfos_online_status)
    ImageView ivStatus;

    @ViewById(R.id.tv_binfos_nowProduction)
    TextView nowProduction;

    @ViewById(R.id.tv_binfos_edayproduction)
    TextView eDayProduction;

    SolarStations_db thisSolarStation;

    @Bean
    SolarStations_Controller solarStationsController;

    @AfterViews
    protected void run(){
        if (thisSolarStation == null){
            thisSolarStation = SessionSingleton.getInstance().getSessionSolarStation();
        }

        configToolbar(thisSolarStation.getName());

        if (thisSolarStation.isOnline()){
            ivStatus.setImageResource(R.mipmap.ic_online);
            nowProduction.setText(thisSolarStation.getPotencia()+"kWh");
        } else {
            ivStatus.setImageResource(R.mipmap.ic_red_dot);
            nowProduction.setText("--");
        }

        eDayProduction.setText(thisSolarStation.getEDay()+thisSolarStation.getEDayUnit());
    }



    private void configToolbar(String title){
        toolbar.setTitle(title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @OptionsItem(R.id.bt_daily_power)
    protected void showDailyGraph(){
        Intent intent = new Intent(getApplicationContext(),GraphsSolarInfosActivity_.class);
        startActivity(intent);
    }
}
