package daniel.vieira.Activity.Fragments;

import android.support.v4.app.Fragment;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Random;

import daniel.vieira.Database.Controller.TimePower_Controller;
import daniel.vieira.Interfaces.SessionSingleton;
import daniel.vieira.Models.SolarStations_db;
import daniel.vieira.Models.TimePower_db;
import daniel.vieira.R;
import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.AxisValue;
import lecho.lib.hellocharts.model.Line;
import lecho.lib.hellocharts.model.LineChartData;
import lecho.lib.hellocharts.model.PointValue;
import lecho.lib.hellocharts.util.ChartUtils;
import lecho.lib.hellocharts.view.LineChartView;

/**
 * Created by danie on 25-Apr-17.
 */

@EFragment(R.layout.fragment_pot_graph)
public class PotGraphFragment extends Fragment{

    @Bean
    TimePower_Controller timePowerController;

    private SolarStations_db thisSolarStation;
    private List<TimePower_db> timePower;

    @ViewById
    LineChartView potChart;

    private LineChartData lineData;

    @AfterViews
    protected void run(){

        if (thisSolarStation == null || timePower == null){
            thisSolarStation = SessionSingleton.getInstance().getSessionSolarStation();
            int tempYear = 2017 - 1900;
            Date testday = new Date(tempYear,6,22,0,0,0);
            timePower = timePowerController.queryForDate(thisSolarStation,testday);
        }

        List<AxisValue> axisValues = new ArrayList<>();
        List<PointValue> values = new ArrayList<>();

        for(int i = 0,j = 0; i < timePower.size(); i++){
            boolean puton = true;
            TimePower_db read = timePower.get(i);
            float y = read.getPower();
            if (y == 0){
                TimePower_db before = new TimePower_db();
                TimePower_db after = new TimePower_db();
                try {
                     before = timePower.get(i-1);
                } catch (ArrayIndexOutOfBoundsException e){
                    before.setPower(0);
                }
                try {
                    after = timePower.get(i-1);
                } catch (ArrayIndexOutOfBoundsException e){
                    after.setPower(0);
                }
                if (before.getPower() == 0 && after.getPower() == 0){
                    puton = false;
                }
            }
            if (puton){
                values.add(new PointValue(j , y )); //talvez deva mudar o 'j'
                Date readDate = read.getTime();
                axisValues.add(new AxisValue(j).setLabel(readDate.getHours()+":"+readDate.getMinutes()));
                j++;
            }
        }

        Line line = new Line(values);
        line.setColor(ChartUtils.COLOR_RED);
        line.setFilled(true);

        List<Line> lines = new ArrayList<>();
        lines.add(line);

        lineData = new LineChartData(lines);
        lineData.setAxisXBottom(new Axis(axisValues).setHasLines(true));
        lineData.setAxisYLeft(new Axis().setHasLines(true).setMaxLabelChars(3));

        potChart.setLineChartData(lineData);
    }

}
