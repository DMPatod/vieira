package daniel.vieira.Activity.Fragments;

import android.support.v4.app.Fragment;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import daniel.vieira.R;
import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.Column;
import lecho.lib.hellocharts.model.ColumnChartData;
import lecho.lib.hellocharts.model.SubcolumnValue;
import lecho.lib.hellocharts.util.ChartUtils;
import lecho.lib.hellocharts.view.ColumnChartView;

/**
 * Created by danie on 25-Apr-17.
 */

@EFragment(R.layout.fragment_hist_graph)
public class HistGraphFragment extends Fragment{

    @ViewById
    ColumnChartView histGraph;

    private ColumnChartData columnData;

    @AfterViews
    protected void run(){
        List<Column> columns = new ArrayList<>();
        List<SubcolumnValue> values;

        Random rand = new Random();
        int min = 1;
        int max = 15;

        for (int i = 0; i < 7; i++){
            values = new ArrayList<>();

            int y = rand.nextInt( (max-min) + 1 ) + min;
            values.add(new SubcolumnValue(y, ChartUtils.pickColor()));

            Column column = new Column(values);
            column.setHasLabels(true);
            column.setHasLabelsOnlyForSelected(true);

            columns.add(column);
        }

        columnData = new ColumnChartData(columns);

        Axis axisX = new Axis().setName("Dias da Semana");
        Axis axisY = new Axis().setHasLines(true).setName("Valor");
        columnData.setAxisXBottom(axisX);
        columnData.setAxisYLeft(axisY);

        histGraph.setColumnChartData(columnData);
    }
}
