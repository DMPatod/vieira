package daniel.vieira.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.sharedpreferences.Pref;

import daniel.vieira.Interfaces.MyPreferences_;
import daniel.vieira.R;

@EActivity(R.layout.activity_splash)
public class SplashActivity extends AppCompatActivity {

    @Pref
    MyPreferences_ myPref;

    @AfterViews
    protected void start(){

        if ( myPref.isLogInBOOL().get()){
            Intent intent = new Intent(this, SolarStationsListActivity_.class);
            startActivity(intent);
            finish();
        } else {
            Intent intent = new Intent(this, LoginActivity_.class);
            startActivity(intent);
            finish();
        }
    }

}
