package daniel.vieira.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.sharedpreferences.Pref;

import daniel.vieira.Database.Controller.SolarStations_Controller;
import daniel.vieira.Interfaces.MyPreferences_;
import daniel.vieira.Interfaces.SessionSingleton;
import daniel.vieira.Interfaces.TestUser;
import daniel.vieira.R;

@EActivity(R.layout.activity_login)
public class LoginActivity extends AppCompatActivity {

    @Bean
    SolarStations_Controller solarStationsController;

    @ViewById(R.id.et_Login_Usuario)
    EditText etUsuario;

    @ViewById(R.id.et_Login_Senha)
    EditText etSenha;

    @ViewById(R.id.cb_keepLogged)
    CheckBox keepLogged;

    @Pref
    MyPreferences_ myPref;

    @Click
    protected void bt_Login_Entrar(){
        String usuario = etUsuario.getText().toString();
        String senha = etSenha.getText().toString();

        if (usuario.equals(TestUser.USERTESTNAME) && senha.equals(TestUser.USERTESTPASSWORD)){

            if (keepLogged.isChecked()){
                myPref.isLogInBOOL().put(true);
            }

            solarStationsController.createTestDB();

            SessionSingleton.getInstance().setUserName(usuario);
//            SET USER EMAIL
//            SessionSingleton.getInstance().setUserEmail(ALGUMA COISA);
            SessionSingleton.getInstance().setUserPassword(senha);

            Intent intent = new Intent(getApplicationContext(), SolarStationsListActivity_.class);
            startActivity(intent);
            finish();
        } else {
            Toast.makeText(getApplicationContext(),"Usuario/Senha invalida",Toast.LENGTH_SHORT).show();
            etUsuario.setText("");
            etSenha.setText("");
            keepLogged.setChecked(false);
        }
    }
}
