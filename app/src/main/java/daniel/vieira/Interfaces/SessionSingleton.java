package daniel.vieira.Interfaces;

import daniel.vieira.Models.SolarStations_db;

/**
 * Created by danie on 22-Jun-17.
 */

public class SessionSingleton {
    private static final SessionSingleton ourInstance = new SessionSingleton();

    public static SessionSingleton getInstance() {
        return ourInstance;
    }

    private SessionSingleton() {
    }

    private String userName = null;

    private String userPassword = null;

    private String userEmail = null;

    private SolarStations_db sessionSolarStation = null;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public SolarStations_db getSessionSolarStation() {
        return sessionSolarStation;
    }

    public void setSessionSolarStation(SolarStations_db sessionSolarStation) {
        this.sessionSolarStation = sessionSolarStation;
    }
}
