package daniel.vieira.Interfaces;

import org.androidannotations.annotations.sharedpreferences.DefaultBoolean;
import org.androidannotations.annotations.sharedpreferences.SharedPref;

/**
 * Created by Cyro on 08/06/2017.
 */

@SharedPref
public interface MyPreferences {

    @DefaultBoolean(false)
    boolean isLogInBOOL();

}
