package daniel.vieira.Models;

import android.content.Context;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import daniel.vieira.R;

/**
 * Created by danie on 12-Apr-17.
 */


@EViewGroup(R.layout.item_list_solarstations)
public class SolarStations_View extends LinearLayout {

    @ViewById
    ImageView solarStationStatusIndicate;

    @ViewById
    TextView solarStationName;

    public SolarStations_View(Context context) {
        super(context);
    }

    public void bind(SolarStations_db solarStation){
        solarStationName.setText(solarStation.getName());

        if (solarStation.isOnline()){
            solarStationStatusIndicate.setImageResource(R.mipmap.ic_green_dot);
        } else {
            solarStationStatusIndicate.setImageResource(R.mipmap.ic_red_dot);
        }
    }
}
