package daniel.vieira.Models;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

/**
 * Created by danie on 22-Jun-17.
 */

@DatabaseTable(tableName = "timepower")
public class TimePower_db {

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(columnName = "time", canBeNull = false, dataType = DataType.DATE_LONG)
    private Date time;

    @DatabaseField(columnName = "power", canBeNull = false)
    private float power;

    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = "solarstation_id")
    private SolarStations_db solarStation;

    public TimePower_db(){
    }

    public TimePower_db(SolarStations_db solarStation, Date time, float power){
        this.solarStation = solarStation;
        this.time = time;
        this.power = power;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public float getPower() {
        return power;
    }

    public void setPower(float power) {
        this.power = power;
    }

    public SolarStations_db getSolarStation() {
        return solarStation;
    }

    public void setSolarStation(SolarStations_db solarStation) {
        this.solarStation = solarStation;
    }

}
