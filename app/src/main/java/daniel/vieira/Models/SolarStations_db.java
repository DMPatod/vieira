package daniel.vieira.Models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Collection;

/**
 * Created by danie on 12-Apr-17.
 */

@DatabaseTable(tableName = "solarStations")
public class SolarStations_db {

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(columnName = "name", canBeNull = false)
    private String name;

    @DatabaseField(columnName = "online")
    private boolean online;

    @DatabaseField(columnName = "potencia")
    private double potencia;

    @DatabaseField(columnName = "EDay")
    private double EDay;

    @DatabaseField(columnName = "EDayUnit")
    private String EDayUnit;

    @ForeignCollectionField
    private Collection<TimePower_db> timePower_table;

    public SolarStations_db(){
    }

    public SolarStations_db(String name, boolean online, double Potencia, double EDay, String EDayUnit){
        this.name = name;
        this.online = online;
        this.potencia = Potencia;
        this.EDay = EDay;
        this.EDayUnit = EDayUnit;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isOnline() {
        return online;
    }

    public void setOnline(boolean online) {
        this.online = online;
    }

    public double getPotencia() {
        return potencia;
    }

    public void setPotencia(double potencia) {
        this.potencia = potencia;
    }

    public double getEDay() {
        return EDay;
    }

    public void setEDay(double EDay) {
        this.EDay = EDay;
    }

    public String getEDayUnit() {
        return EDayUnit;
    }

    public void setEDayUnit(String EDayUnit) {
        this.EDayUnit = EDayUnit;
    }

    public Collection<TimePower_db> getTimePower_table() {
        return timePower_table;
    }

    public void setTimePower_table(Collection<TimePower_db> timePower_table) {
        this.timePower_table = timePower_table;
    }
}
