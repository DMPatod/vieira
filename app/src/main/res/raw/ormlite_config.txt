#
# generated on 2017/07/19 10:06:47
#
# --table-start--
dataClass=daniel.vieira.Models.SolarStations_db
tableName=solarStations
# --table-fields-start--
# --field-start--
fieldName=id
generatedId=true
# --field-end--
# --field-start--
fieldName=name
columnName=name
canBeNull=false
# --field-end--
# --field-start--
fieldName=online
columnName=online
# --field-end--
# --field-start--
fieldName=potencia
columnName=potencia
# --field-end--
# --field-start--
fieldName=EDay
columnName=EDay
# --field-end--
# --field-start--
fieldName=EDayUnit
columnName=EDayUnit
# --field-end--
# --field-start--
fieldName=timePower_table
foreignCollection=true
# --field-end--
# --table-fields-end--
# --table-end--
#################################
# --table-start--
dataClass=daniel.vieira.Models.TimePower_db
tableName=timepower
# --table-fields-start--
# --field-start--
fieldName=id
generatedId=true
# --field-end--
# --field-start--
fieldName=time
columnName=time
dataPersister=DATE_LONG
canBeNull=false
# --field-end--
# --field-start--
fieldName=power
columnName=power
canBeNull=false
# --field-end--
# --field-start--
fieldName=solarStation
columnName=solarstation_id
foreign=true
foreignAutoRefresh=true
maxForeignAutoRefreshLevel=2
# --field-end--
# --table-fields-end--
# --table-end--
#################################
# --table-start--
dataClass=daniel.vieira.Models.Warnings_db
tableName=warnings
# --table-fields-start--
# --field-start--
fieldName=id
generatedId=true
# --field-end--
# --table-fields-end--
# --table-end--
#################################
